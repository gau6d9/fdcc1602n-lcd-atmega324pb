/**
 * Gavin Underdown
 * FDCC1602N LCD Display Controller header.
 * 5 March 2019
*/


#ifndef LCD_H
#define LCD_H
#include <avr/io.h>
#include "LCDCommands.h"

#ifndef F_CPU
	#define F_CPU 16000000UL
#endif
#include <util/delay.h>

// setup aliases.
#ifndef LCD_DATA
	#define LCD_DATA PORTD
	#warning "LCD Data bus not set -- using PORTD"
#endif

#ifndef LCD_DATA_DIR_REG
	#define LCD_DATA_DIR_REG DDRD
	#warning "LCD Data direction register not set -- using DDRD"
#endif

#ifndef LCD_CONTROL_REG 
	#define LCD_CONTROL_REG PORTE
	#warning "LCD Control Bus not set -- using PORTE"
#endif

#ifndef LCD_ENABLE
	#define LCD_ENABLE PINE5
	#warning "LCD Enable line not set -- using PINE-5"
#endif

#ifndef LCD_REG_SELECT
	#define LCD_REG_SELECT PINE2
	#warning "LCD Register Select Line not set -- using PINE-2"
#endif

#ifndef LCD_READ_WRITE
	#define LCD_READ_WRITE PINE3
	#warning "LCD Read/Write Line not set -- using PINE-3"
#endif


/**
 * @brief  Class for LCD Display
 * @note   Assumes wiring has been defined. 
 */
class LcdDisplay {
	public:
	/**
	 * @brief  Initializes the display.
	 * @retval None
	 */
	void init(void);
	/**
	 * @brief  Writes a byte to the command register. 
	 * @param  c: unsigned byte to write to register.
	 * @retval None
	 */
	void write_command(unsigned char c);
	/**
	 * @brief  Writes a character to the display.
	 * @note   Written at the current cursor position.
	 * @param  c: Character to write. 
	 * @retval None
	 */
	void write_char(unsigned char c);
	/**
	 * @brief  Sets the cursor location on the screen.
	 * @note   
	 * @param  row: Which row of the display to put the cursor at.
	 * @param offset: Horizontal offset of the cursor.
	 */
	void set_cursor_loc(uint8_t row, uint8_t offset);
	/**
	 * @brief  Gets a C-string of what is currently displayed on the screen.
	 * @retval C-String of the current screen contents.
	 */
	char* read_screen(void);
	/**
	 * @brief  Reads a character from the screen at a given position.
	 * @param  row: Which row of the display to grab the character from.
	 * @param offset: Horizontal offset of the character to read.
	 * @retval The character at the position specified.
	 */
	char read_char(uint8_t row, uint8_t offset);
	/**
	 * @brief  Sets the backlight on or off
	 * @param  value: Intended value of the backlight (true = on)
	 * @retval None
	 */
	void set_backlight(bool value);
	/**
	 * @brief  Writes a string to the display.
	 * @note   Relies on a null terminator!
	 * @param  str: C-string to write.
	 * @param  row: Which row to write to (0 indexed)
	 * @param  col: Horizontal offset to start string at. 
	 * @retval None
	 */
	void write_string(char * str,  unsigned char row, unsigned char col);
	/**
	 * @brief  Sets the cursor/DRAM address.
	 * @note   Addr must be less than 0x80
	 * @param addr New address.  
	 * @retval None
	 */
	void set_dram_addr(unsigned char addr);
	/**
	 * @brief  Shifts the entire display left.
	 */
	void shift_display_left();
	/**
	 * @brief  Shifts the entire display right.
	 */
	void shift_display_right();
	/**
	 * @brief  Checks to see if the display is busy.
	 * @note
	 * @retval Bit 7 is the busy flag. The other bits are the current address counter.
	 */
	unsigned char is_busy();
	/**
	 * @brief  Clears the display.
	 */
	void clear_display();
};

#endif