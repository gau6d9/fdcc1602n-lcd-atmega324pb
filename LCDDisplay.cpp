/**
 * Gavin Underdown
 * FDCC1602N LCD Display Controller Implementation.
 * 5 March 2019
*/

#include "LCDDisplay.h"

/**
 * @brief  Initializes the display.
 */
void LcdDisplay::init(void) {
	LCD_DATA = 0x00;
	LCD_DATA_DIR_REG = 0xFF;
	LCD_CONTROL_REG = 0x00;
	write_command(0x30); 
	_delay_ms(5);
	write_command(0x30);
	_delay_ms(1);
	write_command(0x30);
	_delay_ms(10);
	
	write_command(0x38);	// set font and lines.
	write_command(0x0C);
	write_command(0x01);
	write_command(0x06);
	return;
}

/**
 * @brief  Writes a byte to the command register. 
 * @param  c: unsigned byte to write to register.
 * @retval None
 */
void LcdDisplay::write_command(unsigned char val) {
	LCD_DATA_DIR_REG = 0xFF;	// ensure that the data pins are outputs.
	LCD_CONTROL_REG = 0x00;	// so it doesn't write to it when we're not ready.
	LCD_DATA = val;
	LCD_CONTROL_REG &= ~(1 << LCD_REG_SELECT);
	LCD_CONTROL_REG &= ~(1 << LCD_READ_WRITE);
	LCD_CONTROL_REG |= (1 << LCD_ENABLE);
	
	_delay_ms(5);	// delay to let it write.
		
	LCD_CONTROL_REG = 0x00;
	LCD_DATA = 0x00;
	return;
}

/**
 * @brief  Writes a character to the display.
 * @note   Written at the current cursor position.
 * @param  c: Character to write. 
 * @retval None
 */
void LcdDisplay::write_char(unsigned char val) {
	LCD_DATA_DIR_REG = 0xFF;	// ensure that the data pins are outputs.
	LCD_DATA = val;
	LCD_CONTROL_REG |= (1 << LCD_REG_SELECT);	// write to data register.
	LCD_CONTROL_REG &= ~(1 << LCD_READ_WRITE);	// tell it to write
	
	LCD_CONTROL_REG |= (1 << LCD_ENABLE);
	_delay_ms(5);	// delay to let it write.
	
	LCD_CONTROL_REG = 0x00;
	LCD_DATA = 0x00;
	return;
}

/**
 * @brief  Sets the backlight on or off
 * @param  value: Intended value of the backlight (true = on)
 * @retval None
 */
void LcdDisplay::set_backlight(bool value) {
	DDRC |= (1 << PINC1);
	PINC &= ~(1 << PINC1);
}

/**
 * @brief  Writes a string to the display.
 * @note   Relies on a null terminator!
 * @param  str: C-string to write.
 * @param  row: Which row to write to (0 indexed)
 * @param  col: Horizontal offset to start string at. 
 * @retval None
 */
void LcdDisplay::write_string(char * str, unsigned char row = 0, unsigned char col = 0) {
	// yay abstraction!
	set_cursor_loc(row, col);

	for(int i = 0; str[i] != '\0' ; ++i) {
		write_char(str[i]);
	}
}

/**
 * @brief  Shifts the entire display right.
 */
void LcdDisplay::shift_display_right() {
	// see page 14 datasheet
	write_command(0b00011100);
}

/**
 * @brief  Shifts the entire display left.
 */
void LcdDisplay::shift_display_left() {
	// see page 14 datasheet
	write_command(0b00011000);
}

/**
 * @brief  Sets the cursor/DRAM address.
 * @note   Addr must be less than 0x80
 * @param addr New address.  
 * @retval None
*/
void LcdDisplay::set_dram_addr(unsigned char addr) {
	unsigned char c = 0x80;	// bit 7 is high for this. 
	c |= addr;
	write_command(c);
}

/**
 * @brief  Sets the cursor location on the screen.
 * @note
 * @param  row: Which row of the display to put the cursor at.
 * @param offset: Horizontal offset of the cursor.
*/
void LcdDisplay::set_cursor_loc(uint8_t row, uint8_t offset){
	// first row address ranges from 0x00 - 0x27
	// second row ranges from 0x40 - 0x67
	if (!row)
		set_dram_addr(0x00 + offset);
	else 
		set_dram_addr(0x40 + offset);
	return;
}

/**
 * @brief  Checks to see if the display is busy.
 * @retval Bit 7 is the busy flag. The other bits are the current address counter.
*/
unsigned char LcdDisplay::is_busy() {
	LCD_DATA_DIR_REG = 0x00;	// ensure that the data pins are inputs
	
	LCD_CONTROL_REG = 0x00;		// so it doesn't write to it when we're not ready.
	
	LCD_CONTROL_REG &= ~(1 << LCD_REG_SELECT);
	LCD_CONTROL_REG &= (1 << LCD_READ_WRITE);
	LCD_CONTROL_REG |= (1 << LCD_ENABLE);
	
	unsigned char val = LCD_DATA;
	return val;
}

/**
 * @brief Clears the display.
*/
void LcdDisplay::clear_display() {
	LCD_DATA_DIR_REG = 0xFF;	// ensure that the data pins are outputs.
	LCD_CONTROL_REG = 0x00;	// so it doesn't write to it when we're not ready.
	LCD_DATA = 0x01;
	LCD_CONTROL_REG &= ~(1 << LCD_REG_SELECT);
	LCD_CONTROL_REG &= ~(1 << LCD_READ_WRITE);
	LCD_CONTROL_REG |= (1 << LCD_ENABLE);
		
	_delay_ms(5);	// delay to let it write.
		
	LCD_CONTROL_REG = 0x00;
	LCD_DATA = 0x00;
	return;
}

/**
 * @brief  Reads a character from the screen at a given position.
 * @param  row: Which row of the display to grab the character from.
 * @param offset: Horizontal offset of the character to read.
 * @retval The character at the position specified.
*/
char LcdDisplay::read_char(uint8_t row, uint8_t offset) {
	set_cursor_loc(row, offset);
	
	LCD_DATA_DIR_REG = 0x00;
	LCD_CONTROL_REG = 0x00;
	
	LCD_CONTROL_REG |= (1 << LCD_REG_SELECT);	// write to data register.
	LCD_CONTROL_REG &= (1 << LCD_READ_WRITE);	// tell it to write
	
	char c = 0x00;
	
	LCD_CONTROL_REG |= (1 << LCD_ENABLE);	// read!
	
	c = LCD_DATA;
	return c;
}